package student.api.studentfingerprint.Daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import student.api.studentfingerprint.Model.Voting;

@Dao
public interface VotingDao {

    @Query("SELECT * FROM Voting WHERE Voting.user_id LIKE :id")
    Voting getUserVote(int id);

    @Insert()
    void insertVoting(Voting userVoting);

    @Delete()
    void deleteVoting(Voting userVoting);
} 
