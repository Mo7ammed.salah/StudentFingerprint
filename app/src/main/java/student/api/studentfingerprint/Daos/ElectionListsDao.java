package student.api.studentfingerprint.Daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import student.api.studentfingerprint.Model.ElectionLists;

@Dao
public interface ElectionListsDao {
    @Query("SELECT * FROM ElectionLists")
    List<ElectionLists> getLists();

    @Insert()
    void insertList(ElectionLists... lists);

    @Delete()
    void deleteList(ElectionLists electionLists);
}
