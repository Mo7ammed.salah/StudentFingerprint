package student.api.studentfingerprint.Daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import student.api.studentfingerprint.Model.User;

/**
 * Created by helio on 2/26/2018.
 */

@Dao
public interface UserDao {

    @Query("SELECT * FROM User")
    List<User> getUsers();

    @Query("SELECT * FROM User WHERE id LIKE :mId")
    User getUser(int mId);
    @Insert()
    void insertUser(User... user);

    @Delete()
    void deleteUser(User user);
}
