package student.api.studentfingerprint.Daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import student.api.studentfingerprint.Model.Persons;


@Dao
public interface PersonsDao {
    @Query("SELECT * FROM Persons")
    List<Persons> getPersons();

    @Insert()
    void insertPerson(Persons... persons);

    @Delete()
    void deletePerson(Persons persons);
}
