package student.api.studentfingerprint.Data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import student.api.studentfingerprint.Daos.ElectionListsDao;
import student.api.studentfingerprint.Daos.PersonsDao;
import student.api.studentfingerprint.Daos.UserDao;
import student.api.studentfingerprint.Daos.VotingDao;
import student.api.studentfingerprint.Model.ElectionLists;
import student.api.studentfingerprint.Model.Persons;
import student.api.studentfingerprint.Model.User;
import student.api.studentfingerprint.Model.Voting;

/**
 * Created by helio on 2/26/2018.
 */

@Database(entities = {User.class, Voting.class, Persons.class, ElectionLists.class},version = 3,exportSchema = true)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public abstract UserDao userDao();
    public abstract VotingDao votingDao();
    public abstract ElectionListsDao electionListsDao();
    public abstract PersonsDao personsDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "user-database")
                            // allow queries on the main thread.
                            // Don't do this on a real app! See PersistenceBasicSample for an example.
                            .allowMainThreadQueries()
                            .fallbackToDestructiveMigration()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
