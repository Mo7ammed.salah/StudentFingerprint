package student.api.studentfingerprint.Controllers;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;

import student.api.studentfingerprint.Views.Fragments.FragmentWalkThorough;

/**
 * Created by mohammedsalah on 11/12/17.
 */

public class IntroPagerAdapter extends AbstractFragmentStepAdapter {


    private static int NUM_ITEMS = 5;

    public IntroPagerAdapter(FragmentManager mManager,Context context) {
        super(mManager,context);
    }

    @Override
    public Step createStep(int position) {
        switch (position){
            case 0 :// Fragment # 1 - This will show FirstFragment
                return FragmentWalkThorough.newInstance(position);
            case 1: // Fragment # 2 - This will show SecondFragment
                return FragmentWalkThorough.newInstance(position);
            case 2: // Fragment # 3 - This will show ThirdFragment
                return FragmentWalkThorough.newInstance(position);
            case 3: // Fragment # 3 - This will show LastFragment
                return FragmentWalkThorough.newInstance(position);
            case 4: // Fragment # 3 - This will show LastFragment
                return FragmentWalkThorough.newInstance(position);
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
