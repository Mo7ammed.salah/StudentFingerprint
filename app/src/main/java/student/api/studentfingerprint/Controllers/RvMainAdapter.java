package student.api.studentfingerprint.Controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import student.api.studentfingerprint.Model.ElectionLists;
import student.api.studentfingerprint.R;
import student.api.studentfingerprint.Utils.CircularImageView;
import student.api.studentfingerprint.Views.Activities.ActivityList;

/**
 * Created by mohammedsalah on 12/20/17.
 */

public class RvMainAdapter extends RecyclerView.Adapter<RvMainAdapter.MyViewHolder> {

    private List<ElectionLists> mList;

    public RvMainAdapter(List<ElectionLists> mList) {
        this.mList = mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_name_list,parent,false);

        return new MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        //TODO set the name
        holder.txtName.setText(mList.get(position).getName());
        //TODO set the No
        holder.txtNo.setText(String.valueOf(mList.get(position).getNo()));
        //TODO set the image of the list item
        Picasso.with(holder.itemView.getContext()).load(mList.get(position).getImagePath()).into(holder.imgListImage);

        //TODO set the item onClick event
        holder.parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle data = new Bundle();
                data.putString("name",mList.get(position).getName());
                data.putString("image",mList.get(position).getImagePath());
                Intent intent = new Intent(holder.itemView.getContext(), ActivityList.class);
                intent.putExtras(data);
                holder.itemView.getContext().startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        //TODO return the size of the list
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        //TODO create my view component
        @BindView(R.id.imgList) CircularImageView imgListImage;
        @BindView(R.id.txtNameList) TextView txtName;
        @BindView(R.id.txtNameNo) TextView txtNo;
        @BindView(R.id.parentView) RelativeLayout parentView;

        public MyViewHolder(View view){
            super(view);
            //TODO binding the view with my view holder
            ButterKnife.bind(this,view);

        }
    }
}
