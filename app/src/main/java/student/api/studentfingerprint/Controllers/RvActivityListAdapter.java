package student.api.studentfingerprint.Controllers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import student.api.studentfingerprint.Model.ElectionLists;
import student.api.studentfingerprint.Model.Persons;
import student.api.studentfingerprint.R;
import student.api.studentfingerprint.Utils.CircularImageView;
import student.api.studentfingerprint.Views.Activities.ActivityFingerprint;

/**
 * Created by mohammedsalah on 12/20/17.
 */

public class RvActivityListAdapter extends RecyclerView.Adapter<RvActivityListAdapter.MyViewHolder> {

    private List<Persons> mList;

    public RvActivityListAdapter(List<Persons> mList) {
        this.mList = mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_name_list,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        //TODO set the name
        holder.txtName.setText(mList.get(position).getFullName());
        //TODO set the No
        holder.txtNo.setText(String.valueOf(mList.get(position).getNo()));
        //TODO set the image of the list item
        Picasso.with(holder.itemView.getContext()).load(mList.get(position).getImage()).into(holder.imgListImage);

        //TODO set the item onClick event
        holder.parentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), ActivityFingerprint.class);
                Bundle data = new Bundle();
                data.putInt("personId",mList.get(position).getId());
                data.putInt("electionId",mList.get(position).getElectionId());
                intent.putExtras(data);
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        //TODO create my view component
        @BindView(R.id.imgList)
        CircularImageView imgListImage;
        @BindView(R.id.txtNameList)
        TextView txtName;
        @BindView(R.id.txtNameNo) TextView txtNo;
        @BindView(R.id.parentView)
        RelativeLayout parentView;

        public MyViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);
        }
    }
}
