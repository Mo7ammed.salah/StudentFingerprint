package student.api.studentfingerprint.Views.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.view.Window;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import student.api.studentfingerprint.Controllers.RvMainAdapter;
import student.api.studentfingerprint.Data.AppDatabase;
import student.api.studentfingerprint.Model.ElectionLists;
import student.api.studentfingerprint.Model.User;
import student.api.studentfingerprint.R;
import student.api.studentfingerprint.Utils.CircularImageView;
import student.api.studentfingerprint.Utils.ECDH.Ecc;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.rvNameList)
    RecyclerView rvNameList;
    @BindView(R.id.imgUserImage)CircularImageView userImageView;
    @BindView(R.id.txtUserFullName) TextView txtUserFullName;
    @BindView(R.id.txtUserIdNo) TextView txtUserIdNo;

    private RvMainAdapter mAdapter;
    private List<ElectionLists> mList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        // set an enter transition
        getWindow().setEnterTransition(new Explode());
        // set an exit transition
        getWindow().setExitTransition(new Explode());
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        //setTestData();
        new LoadUserData(this).execute();
        //new LoadElectionListsTask().execute();

    }

    @SuppressLint("StaticFieldLeak")
    class LoadUserData extends AsyncTask<Void,Void,User>{
        private Ecc ecc;
        private String pKey;
        private SharedPreferences pref;
        private int userId;
        private Context context;
        private AppDatabase mDatabase;

        public LoadUserData(Context context){
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pref = context.getSharedPreferences("userData",MODE_PRIVATE);
            ecc = new Ecc();
            pKey = ecc.startECC();
            mDatabase = AppDatabase.getAppDatabase(context);
        }

        @Override
        protected void onPostExecute(User user) {
            if (user != null){
                Picasso.with(context).load(user.getPicture()).into(userImageView);
                String fullName = user.getFirstName() +" " + user.getMiddleName() +" "+
                        user.getLastName() +" "+user.getForthName();
                txtUserFullName.setText(fullName);
                txtUserIdNo.setText("Identity No. : "+user.getIdNo());
                new LoadElectionListsTask().execute();
            }
        }

        @Override
        protected User doInBackground(Void... voids) {
            userId = pref.getInt("userId",0);
            User eUser = mDatabase.userDao().getUser(userId);
            User dUser = new User();
            if (eUser != null){
                dUser.setFirstName(ecc.decryptPlaintext(pKey,eUser.getFirstName()));
                dUser.setMiddleName(ecc.decryptPlaintext(pKey,eUser.getMiddleName()));
                dUser.setLastName(ecc.decryptPlaintext(pKey,eUser.getLastName()));
                dUser.setForthName(ecc.decryptPlaintext(pKey,eUser.getForthName()));
                dUser.setIdNo(ecc.decryptPlaintext(pKey,eUser.getIdNo()));
                dUser.setPicture(eUser.getPicture());
            }
            return dUser;
        }
    }

    @SuppressLint("StaticFieldLeak")
    class LoadElectionListsTask extends AsyncTask<Void,Void,List<ElectionLists>> {
        private AppDatabase mDatabase;
        private Context context;
        private Ecc ecc;
        private String pKey;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDatabase = AppDatabase.getAppDatabase(context);
            ecc = new Ecc();
            pKey = ecc.startECC();
        }

        @Override
        protected void onPostExecute(List<ElectionLists> electionLists) {
            mAdapter = new RvMainAdapter(electionLists);
            rvNameList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            DividerItemDecoration decoration = new DividerItemDecoration(MainActivity.this,DividerItemDecoration.HORIZONTAL);
            rvNameList.setAdapter(mAdapter);
            rvNameList.addItemDecoration(decoration);
        }

        @Override
        protected List<ElectionLists> doInBackground(Void... voids) {
            List<ElectionLists> eElectionList = new ArrayList<>();
            List<ElectionLists> dElectionList = new ArrayList<>();
            eElectionList = mDatabase.electionListsDao().getLists();
            if (eElectionList.size() > 0){
                for (ElectionLists electionLists : eElectionList) {
                    dElectionList.add(new ElectionLists(electionLists.getId(),
                            ecc.decryptPlaintext(pKey,electionLists.getName()),
                            ecc.decryptPlaintext(pKey,electionLists.getNo()),
                            electionLists.getImagePath()
                            ));
                }
            }
            return dElectionList;
        }
    }

    private void setTestData() {
        mList = new ArrayList<>();
        for (int i=1;i<16;i++){
            mList.add(new ElectionLists(i,"Iraq Independant",String.valueOf(i),"https://images.unsplash.com/photo-1500462918059-b1a0cb512f1d?dpr=2&auto=format&fit=crop&w=767&h=1150&q=60&cs=tinysrgb"));
        }
        mAdapter = new RvMainAdapter(mList);
        rvNameList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        DividerItemDecoration decoration = new DividerItemDecoration(MainActivity.this,DividerItemDecoration.HORIZONTAL);
        rvNameList.setAdapter(mAdapter);
        rvNameList.addItemDecoration(decoration);
    }
}
