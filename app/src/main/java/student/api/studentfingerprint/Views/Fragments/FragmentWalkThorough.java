package student.api.studentfingerprint.Views.Fragments;

import android.Manifest;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import butterknife.ButterKnife;
import student.api.studentfingerprint.Daos.UserDao;
import student.api.studentfingerprint.Data.AppDatabase;
import student.api.studentfingerprint.Model.User;
import student.api.studentfingerprint.Model.Voting;
import student.api.studentfingerprint.R;
import student.api.studentfingerprint.Utils.ECDH.Ecc;
import student.api.studentfingerprint.Utils.FingerprintHandler;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;

/**
 * Created by mohammedsalah on 11/12/17.
 */

public class FragmentWalkThorough extends Fragment implements BlockingStep {

    private Bundle data;
    private int fragmentIndex;
    private EditText inputIdentityNo;
    private boolean isComplete = false;
    private boolean userFounded = false;

    // Declare a string variable for the key we’re going to use in our fingerprint authentication
    private static final String KEY_NAME = "fingerPrintSHA";
    private Cipher cipher;
    private KeyStore keyStore;
    private KeyGenerator keyGenerator;
    private TextView textView;
    private FingerprintManager.CryptoObject cryptoObject;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;

    public static FragmentWalkThorough newInstance(int page) {
        FragmentWalkThorough fragmentFirst = new FragmentWalkThorough();
        Bundle args = new Bundle();
        args.putInt("index", page);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        //get fragment index case first or second or third or ..
        data = getArguments();
        fragmentIndex = data.getInt("index",0);
        //create view group to hold
        // the layout
        ViewGroup group;

        //check for the fragment index that came form the activity
        switch (fragmentIndex){
            case 0:
                group = (ViewGroup) inflater.inflate(R.layout.walkthorough,container,false);
                break;
            case 1:
                group = (ViewGroup) inflater.inflate(R.layout.walkthorough_second,container,false);
                break;
            case 2:
                group = (ViewGroup) inflater.inflate(R.layout.walkthorough_third,container,false);
                break;
            case 3:
                group = (ViewGroup) inflater.inflate(R.layout.walkthorough_fingerprint,container,false);
                break;
            case 4:
                group = (ViewGroup) inflater.inflate(R.layout.walkthorough_last,container,false);
                break;
            default:
                group = (ViewGroup) inflater.inflate(R.layout.walkthorough,container,false);
                break;
        }

        //init ButterKnife library Field and method binding
        //bind butter knife with the selected layout
        ButterKnife.bind(this,group);

        return group;
    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        //check if user in input identity card no
        //do the check operation
        //2 is the third fragment because it counter start from 0
        if (fragmentIndex == 2){
            if (doCheckOperation(callback)){
                SharedPreferences preferences = getContext().getSharedPreferences("userData",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("finishedLogin",true);
                editor.apply();
                FingerPrintCheck();
                callback.goToNextStep();
            }
        }else if (fragmentIndex == 3){
            SharedPreferences pref = getActivity().getSharedPreferences("SHA-1",Context.MODE_PRIVATE);
            String key = pref.getString("SHA-1",null);
            if (key == null){
                Snackbar.make(getView(),"Please finish fingerprint)",Snackbar.LENGTH_LONG).show();
            }else {
                callback.goToNextStep();
            }
        }else {
            callback.goToNextStep();
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    private void FingerPrintCheck() {
        //check for android version if less than 23
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Get an instance of KeyguardManager and FingerprintManager//
            keyguardManager =
                    (KeyguardManager) getContext().getSystemService(KEYGUARD_SERVICE);
            fingerprintManager =
                    (FingerprintManager) getContext().getSystemService(FINGERPRINT_SERVICE);

            textView = (TextView) getView().findViewById(R.id.txtViewMessage);

            //Check whether the device has a fingerprint sensor//
            if (!fingerprintManager.isHardwareDetected()) {
                // If a fingerprint sensor isn’t available, then inform the user that they’ll be unable to use your app’s fingerprint functionality//
                textView.setText(R.string.no_fingerprint);
            }
            //Check whether the user has granted your app the USE_FINGERPRINT permission//
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                // If your app doesn't have this permission, then display the following text//
                textView.setText(R.string.no_fingerprint_permission);
            }

            //Check that the user has registered at least one fingerprint//
            if (!fingerprintManager.hasEnrolledFingerprints()) {
                // If the user hasn’t configured any fingerprints, then display the following message//
                textView.setText(R.string.user_not_use_fingerprint);
            }
            //Check that the lockscreen is secured//
            if (!keyguardManager.isKeyguardSecure()) {
                // If the user hasn’t secured their lockscreen with a PIN password or pattern, then display the following text//
                textView.setText(R.string.no_screen_lock);
            } else {
                try {
                    generateKey();
                } catch (FingerprintException e) {
                    e.printStackTrace();
                }

                if (initCipher()) {
                    //If the cipher is initialized successfully, then create a CryptoObject instance//
                    cryptoObject = new FingerprintManager.CryptoObject(cipher);

                    // Here, I’m referencing the FingerprintHandler class that we’ll create in the next section. This class will be responsible
                    // for starting the authentication process (via the startAuth method) and processing the authentication process events//
                    FingerprintHandler helper = new FingerprintHandler(getContext(),1);
                    helper.startAuth(fingerprintManager, cryptoObject);
                }
            }
        }
    }

    //Create the generateKey method that we’ll use to gain access to the Android keystore and generate the encryption key//

    private void generateKey() throws FingerprintException {
        try {
            // Obtain a reference to the Keystore using the standard Android keystore container identifier (“AndroidKeystore”)//
            keyStore = KeyStore.getInstance("AndroidKeyStore");

            //Generate the key//
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            //Initialize an empty KeyStore//
            keyStore.load(null);

            //Initialize the KeyGenerator//
            keyGenerator.init(new

                    //Specify the operation(s) this key can be used for//
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)

                    //Configure this key so that the user has to confirm their identity with a fingerprint each time they want to use it//
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());

            //Generate the key//
            keyGenerator.generateKey();

        } catch (KeyStoreException
                | NoSuchAlgorithmException
                | NoSuchProviderException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException exc) {
            exc.printStackTrace();
            throw new FingerprintException(exc);
        }
    }

    //Create a new method that we’ll use to initialize our cipher//
    public boolean initCipher() {
        try {
            //Obtain a cipher instance and configure it with the properties required for fingerprint authentication//
            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            //Return true if the cipher has been initialized successfully//
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {

            //Return false if cipher initialization failed//
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    private class FingerprintException extends Exception {
        public FingerprintException(Exception e) {
            super(e);
        }
    }

    private boolean doCheckOperation(StepperLayout.OnNextClickedCallback callback) {
        //binding view with fragment
        inputIdentityNo = (EditText)getView().findViewById(R.id.inputIdentityNo);
        //check if input identity card null or nut
        if (inputIdentityNo.getText().length() <= 0){
            //show error message to tell the user that he must enter an valid identity card no
            Snackbar.make(getView(),"Please Enter your identity card No.",Snackbar.LENGTH_LONG).show();
            //return from the checking operation
            return false;
        }else {
            //get the input string to check it with server
            String input = inputIdentityNo.getText().toString();
            checkUserTask task = new checkUserTask(input,callback);
            task.execute();
            //if the user input an valid identity card no

//            if (users != null){
//                for (User user : users) {
//                   long idNo = Long.valueOf(ecc.decryptPlaintext(pKey,user.getIdNo()));
//                   if (input.equals(idNo)){
//                       founded=true;
//                   }
//                }
//                return founded;
//            }else {
//                //tell the user to enter a valid identity card no
//                Snackbar.make(getView(),"your identity card No. is not valid",Snackbar.LENGTH_LONG).show();
//                return false;
//            }
//            if (input.equals("123")){
//                //trigger that user is complete all the operations successfully
//                //isComplete = !isComplete;
//                return true;
//            }
        }
        return userFounded;
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        if (isComplete){
            Snackbar.make(getView(),"Wow :)",Snackbar.LENGTH_LONG).show();
            callback.complete();
        }
    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }

    public class checkUserTask extends AsyncTask<Void,Void,Boolean>{
        private String input;
        private StepperLayout.OnNextClickedCallback callback;
        private User mUser;
        protected ProgressDialog progressDialog;
        private boolean userVoted = false;

        public checkUserTask(String input,StepperLayout.OnNextClickedCallback callback){
            this.input = input;
            this.callback = callback;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Loading");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (!userVoted){
                if (result){
                    FingerPrintCheck();
                    if (progressDialog != null && progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    callback.goToNextStep();
                }else {
                    Snackbar.make(getView(),"your identity card No. is not valid",Snackbar.LENGTH_LONG).show();
                }
            }else {
                Snackbar.make(getView(),"you already gave your vote.",Snackbar.LENGTH_LONG).show();
            }
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            AppDatabase mDb = AppDatabase.getAppDatabase(getContext());
            UserDao userDao = mDb.userDao();
            Ecc ecc = new Ecc();
            String pKey = ecc.startECC();
            List<User> users = userDao.getUsers();
            boolean founded = false;
            if (users.size() > 0){
                for (User user : users) {
                    String idNo = ecc.decryptPlaintext(pKey,user.getIdNo());
                    if (input.equals(idNo)){
                        founded = true;
                        Voting voting = mDb.votingDao().getUserVote(user.getId());
                        if (voting != null){
                            userVoted = true;
                        }else {
                            SharedPreferences preferences = getContext().getSharedPreferences("userData", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putInt("userId",user.getId());
                            editor.apply();
                        }
                        break;
                    }
                }
                return founded;
            }else {
                //tell the user to enter a valid identity card no
                return false;
            }
        }
    }

}
