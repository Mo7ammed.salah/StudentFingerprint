package student.api.studentfingerprint.Views.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import student.api.studentfingerprint.Controllers.RvActivityListAdapter;
import student.api.studentfingerprint.Controllers.RvMainAdapter;
import student.api.studentfingerprint.Data.AppDatabase;
import student.api.studentfingerprint.Model.ElectionLists;
import student.api.studentfingerprint.Model.Persons;
import student.api.studentfingerprint.Model.User;
import student.api.studentfingerprint.R;
import student.api.studentfingerprint.Utils.CircularImageView;
import student.api.studentfingerprint.Utils.ECDH.Ecc;

/**
 * Created by mohammedsalah on 12/20/17.
 */

public class ActivityList extends AppCompatActivity {

    @BindView(R.id.rvNameList)
    RecyclerView rvNameList;
    @BindView(R.id.buBack)View buBack;
    @BindView(R.id.imgUserImage)CircularImageView imgUserImage;
    @BindView(R.id.txtName)TextView txtName;

    private RvActivityListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide slide = new Slide(Gravity.BOTTOM);
            slide.addTarget(R.id.details);
            slide.setInterpolator(AnimationUtils.loadInterpolator(this, android.R.interpolator
                    .linear_out_slow_in));
            slide.setDuration(400);
            getWindow().setEnterTransition(slide);
        }

        ButterKnife.bind(this);

        Bundle data = getIntent().getExtras();
        if (data != null){
            txtName.setText(data.getString("name"));
            Picasso.with(this).load(data.getString("image")).into(imgUserImage);
        }

        buBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //setTestData();
        new LoadPersonsTask().execute();

    }

    @SuppressLint("StaticFieldLeak")
    class LoadPersonsTask extends AsyncTask<Void,Void,List<Persons>> {
        private AppDatabase mDatabase;
        private Context context;
        private Ecc ecc;
        private String pKey;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mDatabase = AppDatabase.getAppDatabase(context);
            ecc = new Ecc();
            pKey = ecc.startECC();
        }

        @Override
        protected void onPostExecute(List<Persons> persons) {
            mAdapter = new RvActivityListAdapter(persons);
            rvNameList.setLayoutManager(new LinearLayoutManager(ActivityList.this));
            DividerItemDecoration decoration = new DividerItemDecoration(ActivityList.this,DividerItemDecoration.HORIZONTAL);
            rvNameList.setAdapter(mAdapter);
            rvNameList.addItemDecoration(decoration);
        }

        @Override
        protected List<Persons> doInBackground(Void... voids) {
            List<Persons> ePersons = new ArrayList<>();
            List<Persons> dPersons = new ArrayList<>();
            ePersons = mDatabase.personsDao().getPersons();
            if (ePersons.size() > 0){
                for (Persons persons : ePersons) {
                    dPersons.add(new Persons(persons.getId(),
                            ecc.decryptPlaintext(pKey,persons.getFullName()),
                            ecc.decryptPlaintext(pKey,persons.getNo()),
                            persons.getImage(),
                            persons.getElectionId()
                    ));
                }
            }
            return dPersons;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityList.this.finishAfterTransition();
    }
}
