package student.api.studentfingerprint.Views.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;


import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import student.api.studentfingerprint.Controllers.IntroPagerAdapter;
import student.api.studentfingerprint.Data.AppDatabase;
import student.api.studentfingerprint.Model.ElectionLists;
import student.api.studentfingerprint.Model.Persons;
import student.api.studentfingerprint.Model.User;
import student.api.studentfingerprint.R;
import student.api.studentfingerprint.Utils.ECDH.Ecc;

/**
 * Created by mohammedsalah on 11/10/17.
 * remember we flagged android:noHistory="true" in the manifest file,so user
 * can not go back to this activity when he accidentally pressed the back button on the main activity
 */

public class WalkThorough extends AppCompatActivity implements StepperLayout.StepperListener{

    @BindView(R.id.stepperLayout)
    StepperLayout mStepper;
    private IntroPagerAdapter introPagerAdapter;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walkthorough);

        //check if the user already finish the log in
        preferences = getSharedPreferences("userData",MODE_PRIVATE);
        boolean finished = false;
        finished = preferences.getBoolean("finishedLogin",false);
        if (finished){
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }
        //bind butter knife with the selected layout
        ButterKnife.bind(this);
        //init intro pager adapter
        introPagerAdapter = new IntroPagerAdapter(getSupportFragmentManager(),this);
        //bind intro pager adapter with Stepper layout
        mStepper.setAdapter(introPagerAdapter);
        mStepper.setCompleteButtonEnabled(true);
        //set the stepper listener onCompleted,onError...
        mStepper.setListener(this);
        AppDatabase mDataBase = AppDatabase.getAppDatabase(this);
        new AddDataTask(mDataBase).execute();

    }

    private void addData(AppDatabase mDb) {

        Ecc ecc = new Ecc();
        //get elliptic curve public key
        String pKey = ecc.startECC();

        User user = new User();
        User user1 = new User();
        User user2 = new User();
        User user3 = new User();

        Persons persons = new Persons();
        Persons persons1 = new Persons();
        Persons persons2 = new Persons();
        Persons persons3 = new Persons();

        ElectionLists electionLists = new ElectionLists();
        ElectionLists electionLists1 = new ElectionLists();
        ElectionLists electionLists2 = new ElectionLists();

        //region Add Users
        user.setFirstName(ecc.encryptPlainText(pKey,"Mohammed"));
        user.setMiddleName(ecc.encryptPlainText(pKey,"salah"));
        user.setLastName(ecc.encryptPlainText(pKey,"Mahdi"));
        user.setForthName(ecc.encryptPlainText(pKey,"Motar"));
        user.setIdNo(ecc.encryptPlainText(pKey,"123"));
        user.setPicture("file:///android_asset/image/users/1.png");
        mDb.userDao().insertUser(user);

        user1.setFirstName(ecc.encryptPlainText(pKey,"Ahmed"));
        user1.setMiddleName(ecc.encryptPlainText(pKey,"Saleh"));
        user1.setLastName(ecc.encryptPlainText(pKey,"Ali"));
        user1.setForthName(ecc.encryptPlainText(pKey,"Mohammed"));
        user1.setIdNo(ecc.encryptPlainText(pKey,"1234"));
        user1.setPicture("file:///android_asset/image/users/2.png");
        mDb.userDao().insertUser(user1);

        user2.setFirstName(ecc.encryptPlainText(pKey,"Dania"));
        user2.setMiddleName(ecc.encryptPlainText(pKey,"Hussien"));
        user2.setLastName(ecc.encryptPlainText(pKey,"Ahmed"));
        user2.setForthName(ecc.encryptPlainText(pKey,"Mahdi"));
        user2.setIdNo(ecc.encryptPlainText(pKey,"12345"));
        user2.setPicture("file:///android_asset/image/users/3.png");
        mDb.userDao().insertUser(user2);

        user3.setFirstName(ecc.encryptPlainText(pKey,"Sama"));
        user3.setMiddleName(ecc.encryptPlainText(pKey,"Ahmed"));
        user3.setLastName(ecc.encryptPlainText(pKey,"Mohammed"));
        user3.setForthName(ecc.encryptPlainText(pKey,"Ali"));
        user3.setIdNo(ecc.encryptPlainText(pKey,"123456"));
        user3.setPicture("file:///android_asset/image/users/4.png");
        mDb.userDao().insertUser(user3);
        //endregion

        //region Add Persons
        //region 1
        persons.setFullName(ecc.encryptPlainText(pKey,"Mariam Mohammed Raheem"));
        persons.setNo(ecc.encryptPlainText(pKey,"1"));
        persons.setImage("file:///android_asset/image/persons/1.png");
        persons.setElectionId(1);
        mDb.personsDao().insertPerson(persons);

        persons1.setFullName(ecc.encryptPlainText(pKey,"Ahmed Mohammed Raheem"));
        persons1.setNo(ecc.encryptPlainText(pKey,"2"));
        persons1.setImage("file:///android_asset/image/persons/2.png");
        persons.setElectionId(1);
        mDb.personsDao().insertPerson(persons1);

        persons2.setFullName(ecc.encryptPlainText(pKey,"Fatima Ahmed Ali"));
        persons2.setNo(ecc.encryptPlainText(pKey,"3"));
        persons2.setImage("file:///android_asset/image/persons/3.png");
        persons.setElectionId(1);
        mDb.personsDao().insertPerson(persons2);

        persons3.setFullName(ecc.encryptPlainText(pKey,"Mohammed Razaq Ahmed"));
        persons3.setNo(ecc.encryptPlainText(pKey,"4"));
        persons3.setImage("file:///android_asset/image/persons/4.png");
        persons.setElectionId(1);
        mDb.personsDao().insertPerson(persons3);
        //endregion

        //region 2
        persons.setFullName(ecc.encryptPlainText(pKey,"Mariam Mohammed Raheem"));
        persons.setNo(ecc.encryptPlainText(pKey,"1"));
        persons.setImage("file:///android_asset/image/persons/1.png");
        persons.setElectionId(2);
        mDb.personsDao().insertPerson(persons);

        persons1.setFullName(ecc.encryptPlainText(pKey,"Ahmed Mohammed Raheem"));
        persons1.setNo(ecc.encryptPlainText(pKey,"2"));
        persons1.setImage("file:///android_asset/image/persons/2.png");
        persons.setElectionId(2);
        mDb.personsDao().insertPerson(persons1);

        persons2.setFullName(ecc.encryptPlainText(pKey,"Fatima Ahmed Ali"));
        persons2.setNo(ecc.encryptPlainText(pKey,"3"));
        persons2.setImage("file:///android_asset/image/persons/3.png");
        persons.setElectionId(2);
        mDb.personsDao().insertPerson(persons2);

        persons3.setFullName(ecc.encryptPlainText(pKey,"Mohammed Razaq Ahmed"));
        persons3.setNo(ecc.encryptPlainText(pKey,"4"));
        persons3.setImage("file:///android_asset/image/persons/4.png");
        persons.setElectionId(2);
        mDb.personsDao().insertPerson(persons3);
        //endregion

        //region 3
        persons.setFullName(ecc.encryptPlainText(pKey,"Mariam Mohammed Raheem"));
        persons.setNo(ecc.encryptPlainText(pKey,"1"));
        persons.setImage("file:///android_asset/image/persons/1.png");
        persons.setElectionId(3);
        mDb.personsDao().insertPerson(persons);

        persons1.setFullName(ecc.encryptPlainText(pKey,"Ahmed Mohammed Raheem"));
        persons1.setNo(ecc.encryptPlainText(pKey,"2"));
        persons1.setImage("file:///android_asset/image/persons/2.png");
        persons.setElectionId(3);
        mDb.personsDao().insertPerson(persons1);

        persons2.setFullName(ecc.encryptPlainText(pKey,"Fatima Ahmed Ali"));
        persons2.setNo(ecc.encryptPlainText(pKey,"3"));
        persons2.setImage("file:///android_asset/image/persons/3.png");
        persons.setElectionId(3);
        mDb.personsDao().insertPerson(persons2);

        persons3.setFullName(ecc.encryptPlainText(pKey,"Mohammed Razaq Ahmed"));
        persons3.setNo(ecc.encryptPlainText(pKey,"4"));
        persons3.setImage("file:///android_asset/image/persons/4.png");
        persons.setElectionId(3);
        mDb.personsDao().insertPerson(persons3);
        //endregion
        //endregion

        //region Add Election lists
        electionLists.setName(ecc.encryptPlainText(pKey,"Election List 1"));
        electionLists.setNo(ecc.encryptPlainText(pKey,"1"));
        electionLists.setImagePath("file:///android_asset/image/electionLists/1.png");
        mDb.electionListsDao().insertList(electionLists);

        electionLists1.setName(ecc.encryptPlainText(pKey,"Election List 2"));
        electionLists1.setNo(ecc.encryptPlainText(pKey,"2"));
        electionLists1.setImagePath("file:///android_asset/image/electionLists/2.png");
        mDb.electionListsDao().insertList(electionLists1);

        electionLists2.setName(ecc.encryptPlainText(pKey,"Election List 3"));
        electionLists2.setNo(ecc.encryptPlainText(pKey,"3"));
        electionLists2.setImagePath("file:///android_asset/image/electionLists/3.png");
        mDb.electionListsDao().insertList(electionLists2);
        //endregion
    }

    @Override
    public void onCompleted(View completeButton) {
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
    }

    @Override
    public void onError(VerificationError verificationError) {
        //show error if any thing wrong
        Snackbar.make(mStepper,verificationError.getErrorMessage(),Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onStepSelected(int newStepPosition) {
        //print the new page stat in the log file for check it in development state
        Log.d("new Step selected ",String.valueOf(newStepPosition));
    }

    @Override
    public void onReturn() {
        //on return pressed button this will finish this activity

    }

    class AddDataTask extends AsyncTask<Void,Void,Void>{
        private AppDatabase mDatabase;
        public AddDataTask(AppDatabase mDatabase){
            this.mDatabase = mDatabase;
        }
        @Override
        protected Void doInBackground(Void... voids) {
            List<User> users = mDatabase.userDao().getUsers();
            if (users.size() <= 0){
                addData(mDatabase);
            }
            return null;
        }
    }
}
