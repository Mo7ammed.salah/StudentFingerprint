package student.api.studentfingerprint.Views.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import student.api.studentfingerprint.R;

/**
 * Created by helio on 28/02/2018.
 */

public class ActivitySplash extends AppCompatActivity {
    private SharedPreferences preferences;
    private boolean finished = false;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        preferences = getSharedPreferences("userData",MODE_PRIVATE);
        finished = preferences.getBoolean("finishedLogin",false);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (finished){
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }else {
                    startActivity(new Intent(getApplicationContext(),WalkThorough.class));
                }
            }
        },2000);

    }
}
