package student.api.studentfingerprint.Model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by mohammedsalah on 12/20/17.
 */
@Entity
public class ElectionLists {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "name")
    private String Name;
    @ColumnInfo(name = "no")
    private String no;
    @ColumnInfo(name = "image_path")
    private String imagePath;

    public ElectionLists(int id, String name, String no, String imagePath) {
        this.id = id;
        Name = name;
        this.no = no;
        this.imagePath = imagePath;
    }
    public ElectionLists(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
