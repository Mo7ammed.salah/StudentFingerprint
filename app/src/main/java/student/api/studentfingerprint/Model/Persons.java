package student.api.studentfingerprint.Model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by helio on 27/02/2018.
 */

@Entity
public class Persons {

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "full_name")
    private String fullName;
    @ColumnInfo(name = "no")
    private String no;
    @ColumnInfo(name = "image")
    private String image;
    @ColumnInfo(name = "election_id")
    private int electionId;

    public Persons(int personId,String name,String no,String image,int electionId){
        this.id=personId;
        this.fullName = name;
        this.no = no;
        this.image = image;
    }

    public Persons(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getElectionId() {
        return electionId;
    }

    public void setElectionId(int electionId) {
        this.electionId = electionId;
    }
}
