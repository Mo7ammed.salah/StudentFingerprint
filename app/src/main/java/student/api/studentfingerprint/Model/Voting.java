package student.api.studentfingerprint.Model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by helio on 01/03/2018.
 */
@Entity
public class Voting {
    @PrimaryKey(autoGenerate =true)
    private int id;
    @ColumnInfo(name = "user_id")
    private int uId;
    @ColumnInfo(name ="election_list_id")
    private String electionId;
    @ColumnInfo(name = "person_id")
    private String personId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUId() {
        return uId;
    }

    public void setUId(int uId) {
        this.uId = uId;
    }

    public String getElectionId() {
        return electionId;
    }

    public void setElectionId(String electionId) {
        this.electionId = electionId;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }
}
