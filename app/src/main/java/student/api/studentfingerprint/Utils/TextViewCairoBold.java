package student.api.studentfingerprint.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import java.lang.reflect.Type;

/**
 * Created by mohammedsalah on 11/10/17.
 */

@SuppressLint("AppCompatCustomView")
public class TextViewCairoBold extends TextView {
    public TextViewCairoBold(Context context) {
        super(context);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"/fonts/Cairo-Bold.ttf");
        this.setTypeface(typeface);
    }

    public TextViewCairoBold(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"/fonts/Cairo-Bold.ttf");
        this.setTypeface(typeface);
    }

    public TextViewCairoBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(),"/fonts/Cairo-Bold.ttf");
        this.setTypeface(typeface);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
