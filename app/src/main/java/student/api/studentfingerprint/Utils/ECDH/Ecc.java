package student.api.studentfingerprint.Utils.ECDH;

import java.math.BigInteger;
import java.util.*;

public class Ecc{
    BigInteger p,a,b,x,d,n;
    ECParameters params;
    ECPoint G,Qs=null,K,Qr=null;
    String k="";

    private AES aes=new AES();

    public String encryptPlainText(String publicKey,String plainText){
        String encrypted="";
        String key[] = publicKey.split("\\.");
        startKey(key[1]);
        encrypted = encrypt(plainText);
        return encrypted;
    }

    public String decryptPlaintext(String publicKey,String encryptedText){
        String plainText = "";
        String encrypted[] = encryptedText.split("\\.");
        String key[] = publicKey.split("\\.");
        startECC();
        startKey(key[1]);
        plainText = decrypt(encrypted[1]);
        return plainText;
    }

    public String startECC() {
        System.out.println("Ecc Started");
        String msgs,pqx,pqy;
        ECPoint pQ;
        try
        {
            iniatilization();
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
        randomSelection();
        pQ=publicValue();
        pqx=pQ.x().toString();
        pqy=pQ.y().toString();
        msgs = "key."+pqx+","+pqy;
        return msgs;
    }

    private void startKey(String rQ) {
        // get public key
        System.out.println("Key Generation Started");
        String[] spl=rQ.split("\\,");
        String Qrx,Qry;
        Qrx=spl[0];
        Qry=spl[1];
        BigInteger qsx=convBigInteger(Qrx);
        BigInteger qsy=convBigInteger(Qry);
        Qr = new ECPoint(qsx,qsy,params);//generate ECPoint public key
        secretValue(Qr);//generate secret
    }

	/*
			Trigger the
			receiver in
			main algorithm
			on android
	   */

    private void iniatilization() throws Exception {
        // Initialization of paramets
        p = new BigInteger ("6277101735386680763835789423207666416083908700390324961279");
        a = new BigInteger ("-3") .mod (p);
        b = new BigInteger ("64210519e59c80e70fa7e9ab72243049feb8deecc146b9b1",16);
        params = new ECParameters (p, a, b);

        // Calculation of Generator Point, n
        G = new ECPoint
                (new BigInteger ("188da80eb03090f67cbf20eb43a18800f4ff0afd82ff10123345",16),
                        new BigInteger ("07192b95ffc8da78631011ed6b24cdd573f977a11e7948112345",16),
                        params);
        n = new BigInteger ("6277101735386680763835789423176059013767194773182842284081");

    }

    private void randomSelection() {
        Random prng;
        long seed = 125L;
        prng = new Random (seed);

        //d is the sender private key
        // Step 1 : Select random number d such that d <n
        d = new BigInteger (192, prng);
        if (d.compareTo (n) > 0)
            d = d.subtract (n);
    }

    private ECPoint publicValue() {
        //Q is the sender public key
        // Step 2: calculate Q= d. G
        Qs = G.multiply (d);
        return (Qs);
    }

    // calculation of K
    private void secretValue(ECPoint Qk) {
        // Sender Side Step 5 : calculation of secret value Ks = d * Qr
        K = Qk.multiply (d);
        String res = (K.x().multiply(K.y())).toString();
        k = res.substring(0,16);
    }

    private String decrypt(String data) {

        aes.setKey(k);  // choose 16 byte password
        System.out.println("Decryption");
        //Decryption
        System.out.println("\nCipher text : ["+data+"] ["+data.length()+" bytes]");
        String unencrypted = ((AES) aes).Decrypt(data);
        System.out.println("\nPlain text : ["+unencrypted+"] ["+unencrypted.length()+" bytes]");
        return unencrypted;
    }

    private String encrypt(String data) {
        System.out.println("Encryption");
        aes.setKey(k);  // choose 16 byte password
        System.out.println("Data:"+data);
        //Encryption

        System.out.println("\nPlain text : ["+data+"] ["+data.length()+" bytes]");
        String encrypted = ((AES) aes).Encrypt(data);
        System.out.println("\nCypher text : ["+encrypted+"] ["+encrypted.length()+" bytes]");
        encrypted = "emg."+encrypted;
        System.out.println("Cypher Size:"+encrypted.length());
        return encrypted;
    }

    private static BigInteger convBigInteger(String str){

        byte [ ] asciiValues = str.getBytes ( );
        return new BigInteger (asciiValues);
    }

    public static String bytesToHex(byte[] b){
        char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        StringBuffer buf = new StringBuffer();
        for (int j=0; j<b.length; j++) {
            buf.append(hexDigit[(b[j] >> 4) & 0x0f]);
            buf.append(hexDigit[b[j] & 0x0f]);
        }
        return buf.toString();
    }

}