package student.api.studentfingerprint.Utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.provider.Settings.Secure;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import student.api.studentfingerprint.Data.AppDatabase;
import student.api.studentfingerprint.Model.Voting;
import student.api.studentfingerprint.Utils.ECDH.Ecc;
import student.api.studentfingerprint.Views.Activities.MainActivity;
import student.api.studentfingerprint.Views.Activities.WalkThorough;

/**
 * Created by mohammedsalah on 11/7/17.
 */

public class FingerprintHandler extends
        FingerprintManager.AuthenticationCallback {

    private CancellationSignal cancellationSignal;
    private Context appContext;
    private int from = 0;
    private Bundle data;

    public FingerprintHandler(Context context,int from) {
        this.appContext = context;
        this.from =from;
    }

    public FingerprintHandler(Context context, int from, Bundle data){
        this.appContext = context;
        this.from = from;
        this.data = data;
    }

    public void startAuth(FingerprintManager manager,
                          FingerprintManager.CryptoObject cryptoObject) {

        cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(appContext,
                Manifest.permission.USE_FINGERPRINT) !=
                PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId,
                                      CharSequence errString) {
        Toast.makeText(appContext,
                "Authentication error\n" + errString,
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId,
                                     CharSequence helpString) {
        Toast.makeText(appContext,
                "Authentication help\n" + helpString,
                Toast.LENGTH_LONG).show();
        Log.d("FingerPrint","Authentication help\n" + helpString);
    }

    @Override
    public void onAuthenticationFailed() {
        Toast.makeText(appContext,
                "Authentication failed.",
                Toast.LENGTH_LONG).show();
        Log.d("FingerPrint","Authentication Failed.");
    }

    @Override
    public void onAuthenticationSucceeded(
            FingerprintManager.AuthenticationResult result) {
            if (from == 1){
                SharedPreferences preferences = appContext.getSharedPreferences("userData",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("finishedLogin",true);
                editor.apply();
                Toast.makeText(appContext,
                        "Authentication succeeded.",
                        Toast.LENGTH_LONG).show();
                appContext.startActivity(new Intent(appContext, MainActivity.class));
            }else if (from == 2){
                AppDatabase mDb = AppDatabase.getAppDatabase(appContext);
                Ecc ecc = new Ecc();
                String pKey = ecc.startECC();
                Voting voting = new Voting();
                SharedPreferences pref= appContext.getSharedPreferences("userData",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                int userId = pref.getInt("userId",0);
                voting.setElectionId(ecc.encryptPlainText(pKey,String.valueOf(data.getInt("electionId"))));
                voting.setPersonId(ecc.encryptPlainText(pKey,String.valueOf(data.getInt("personId"))));
                voting.setUId(userId);
                mDb.votingDao().insertVoting(voting);
                editor.clear();
                editor.apply();
                appContext.startActivity(new Intent(appContext, WalkThorough.class));
            }
    }
}
